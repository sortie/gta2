#ifndef MAP_HXX
#define MAP_HXX

const size_t MAP_WIDTH = 256;
const size_t MAP_BREADTH = 256;
const size_t MAP_HEIGHT = 8;

const uint8_t ARROWS_GREEN_LEFT = 1 << 0;
const uint8_t ARROWS_GREEN_RIGHT = 1 << 1;
const uint8_t ARROWS_GREEN_UP = 1 << 2;
const uint8_t ARROWS_GREEN_DOWN = 1 << 3;
const uint8_t ARROWS_RED_LEFT = 1 << 4;
const uint8_t ARROWS_RED_RIGHT = 1 << 5;
const uint8_t ARROWS_RED_UP = 1 << 6;
const uint8_t ARROWS_RED_DOWN = 1 << 7;

const uint8_t GROUND_TYPE_AIR = 0;
const uint8_t GROUND_TYPE_ROAD = 1;
const uint8_t GROUND_TYPE_PAVEMENT = 2;
const uint8_t GROUND_TYPE_FIELD = 3;
const uint8_t GROUND_TYPE_SHIFT = 0;
const uint8_t GROUND_TYPE_MASK = (4 - 1) << GROUND_TYPE_SHIFT;

const uint8_t SLOPE_TYPE_SHIFT = 2 /* log2(GROUND_TYPE_MASK) */;
const uint8_t SLOPE_TYPE_MASK = (64 - 1) << SLOPE_TYPE_SHIFT;

const uint16_t WALL_TEXTURE_THREE_SIDED_DIAGIONAL_SLOPE = 1023;
const uint16_t WALL_TEXTURE_SHIFT = 0;
const uint16_t WALL_TEXTURE_MASK = (1024-1) << WALL_TEXTURE_SHIFT;
const uint16_t WALL_COLLIDE = 1 << 10;
const uint16_t WALL_COLLIDE_BULLET = 1 << 11;
const uint16_t WALL_LIGHT_MASK = 4 - 1;
const uint16_t WALL_LIGHT_SHIFT = 10;
const uint16_t WALL_FLAT = 1 << 12;
const uint16_t WALL_FLIP = 1 << 13;
const uint16_t WALL_ROTATE_SHIFT = 14;
const uint16_t WALL_ROTATE_MASK = (4 - 1) << WALL_ROTATE_SHIFT;

struct block_info
{
	uint16_t left;
	uint16_t right;
	uint16_t top;
	uint16_t bottom;
	uint16_t lid;
	uint8_t arrows;
	uint8_t slope_type;
};

struct map
{
	struct block_info city_scape[MAP_HEIGHT][MAP_BREADTH][MAP_WIDTH];
};

#if 0

struct col_info_cmap
{
	uint8_t height;
	uint8_t offset;
	uint16_t blockd[height];
};

struct compressed_map_cmap
{
	uint16_t base[MAP_WIDTH];
	uint16_t column_words;
	uint16_t column[column_words];
	uint16_t num_blocks;
	struct block_info block[num_blocks];
};

#endif

struct col_info_dmap
{
	uint8_t height;
	uint8_t offset;
	uint16_t pad;
	uint32_t blockd[];
};

struct compressed_map_part1_dmap
{
	uint32_t base[MAP_BREADTH][MAP_WIDTH];
	uint32_t column_words;
};

//uint16_t column[column_words];

struct compressed_map_part2_dmap
{
	uint32_t num_blocks;
};

//struct block_info block[num_blocks];

const uint8_t ZONE_GENERAL_PURPOSE = 0;
const uint8_t ZONE_NAVIGATION = 1;
const uint8_t ZONE_TRAFFIC_LIGHT = 1;
const uint8_t ZONE_ARROW_BLOCKER = 5;
const uint8_t ZONE_RAILWAY_STATION = 6;
const uint8_t ZONE_BUS_STOP = 7;
const uint8_t ZONE_GENERAL_TRIGGER = 8;
const uint8_t ZONE_INFORMATION = 10;
const uint8_t ZONE_RAILWAY_STATION_ENTRY_POINT = 11;
const uint8_t ZONE_RAILWAY_STATION_EXIT_POINT = 12;
const uint8_t ZONE_RAILWAY_STOP_POINT = 13;
const uint8_t ZONE_GANG = 14;
const uint8_t ZONE_LOCAL_NAVIGATION = 15;
const uint8_t ZONE_RESTART = 16;
const uint8_t ZONE_ARREST_RESTART = 20;

struct map_zone
{
	uint8_t zone_type;
	uint8_t x;
	uint8_t y;
	uint8_t w;
	uint8_t h;
	uint8_t name_length;
	char name[];
};

struct map_object
{
	fix16 x;
	fix16 y;
	ang8 rotation;
	uint8_t object_type;
};

struct mapping_entry
{
	uint8_t tile_number;
	uint8_t undefined;
};

struct tile_animation
{
	uint16_t base;
	uint8_t frame_rate;
	uint8_t repeat;
	uint8_t anim_length;
	uint8_t unused;
	uint16_t tiles[/*anim_length*/];
};

struct map_light
{
	uint8_t b;
	uint8_t g;
	uint8_t r;
	uint8_t a;
	fix16 x;
	fix16 y;
	fix16 z;
	fix16 radius;
	uint8_t intensity;
	uint8_t shape;
	uint8_t on_time;
	uint8_t off_time;
};

#if 0

typedef uint32_t link;

struct junction
{
	link north;
	link south;
	link east;
	link west;
	search_type junc_type;
	uint8_t min_x;
	uint8_t min_y;
	uint8_t max_x;
	uint8_t max_y;
};

struct segment
{
	uint16_t junction_num1;
	uint16_t junction_num2;
	uint8_t min_x;
	uint8_t min_y;
	uint8_t max_x;
	uint8_t max_y;
};

#endif

const char MAP_FILE_HEADER_TYPE[4] = { 'G', 'B', 'M', 'P' };
const uint16_t MAP_FILE_HEADER_VERSION = 500;

struct file_header
{
	char file_type[4];
	uint16_t version_code;
};

const char CHUNK_TYPE_ANIM[4] = { 'A', 'N', 'I', 'M' };
const char CHUNK_TYPE_DMAP[4] = { 'D', 'M', 'A', 'P' };
const char CHUNK_TYPE_LGHT[4] = { 'L', 'G', 'H', 'T' };
const char CHUNK_TYPE_ZONE[4] = { 'Z', 'O', 'N', 'E' };

struct chunk_header
{
	char chunk_type[4];
	uint32_t chunk_size;
};

#endif
