PREFIX?=/usr/local
EXEC_PREFIX?=$(PREFIX)
BINDIR?=$(EXEC_PREFIX)/bin

CXX?=g++

CPPFLAGS?=
CXXFLAGS?=-g -O2

CPPFLAGS:=$(CPPFLAGS)
CXXFLAGS:=$(CXXFLAGS) -Wall -Wextra

BINARY:=gta2
LIBS:=-lSDL -lGLEW -lGLU

all: $(BINARY)

$(BINARY): *.c++ *.h++
	$(CXX) *.c++ -o $(BINARY) $(CXXFLAGS) $(CPPFLAGS) $(LIBS)

clean:
	rm -f $(BINARY)

install: $(BINARY)
	install $(BINARY) $(DESTDIR)$(BINDIR)
