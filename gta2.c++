#include <sys/types.h>

#include <assert.h>
#include <errno.h>
#include <error.h>
#include <locale.h>
#include <math.h>
#include <endian.h>
#include <stddef.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <unistd.h>

#include <GL/glew.h>
#include <GL/glu.h>

#include <SDL/SDL.h>

#include "f3.h++"
#include "f4x4.h++"

typedef uint16_t fix16;
typedef uint8_t ang8; // 128 is 180 degress

#include "map.h++"
#include "style.h++"

float float_of_fix16(fix16 value)
{
	uint16_t mantissa = value & 0x7FFF;
	float ret = (float) mantissa / (1 << 7);
	return value & 0x8000 ? -ret : ret;
}

const size_t NUM_LIGHTS = 32;
const size_t SPRITE_DATA_PAGE_SIZE = 256 * 256 * sizeof(uint8_t);

struct level
{
	struct map map;
	struct map_light* lights;
	uint8_t* light_time_left;
	bool* light_on;
	uint8_t* tile_animation_data;
	size_t tile_animation_size;
	size_t num_lights;
	size_t spawn_x;
	size_t spawn_y;
	size_t num_spawn_zones;
	uint16_t tile_displayed[1024];
	uint8_t tile_current_frame[1024];
	uint8_t tile_frame_time[1024];
	uint8_t tile_repeats[1024];
};

struct style
{
	struct palette_index palette_index;
	struct palette_base palette_base_count;
	struct palette_base palette_base;
	uint32_t palette_data[64][256*64];
	GLuint palette_pages[64];
	uint8_t tile_pages[64][256*256];
	GLuint tiles[1024];
	struct sprite_base sprite_base_count;
	struct sprite_base sprite_base;
	struct sprite_entry* sprite_entries;
	size_t num_sprite_entries;
	uint8_t* sprite_data;
	size_t sprite_data_size;
	size_t num_sprite_pages;
	GLuint* sprite_pages;
};

void fix_file_header_endian(struct file_header* file_header)
{
	file_header->version_code = le16toh(file_header->version_code);
}

void fix_chunk_header_endian(struct chunk_header* chunk_header)
{
	chunk_header->chunk_size = le32toh(chunk_header->chunk_size);
}

void fix_compressed_map_part1_dmap_endian(struct compressed_map_part1_dmap* part)
{
	for ( size_t y = 0; y < MAP_BREADTH; y++ )
		for ( size_t x = 0; x < MAP_WIDTH; x++ )
			part->base[y][x] = le16toh(part->base[y][x]);
	part->column_words = le32toh(part->column_words);
}

void fix_compressed_map_part2_dmap_endian(struct compressed_map_part2_dmap* part)
{
	part->num_blocks = le32toh(part->num_blocks);
}

void fix_block_info_endian(struct block_info* block)
{
	block->left = le16toh(block->left);
	block->right = le16toh(block->right);
	block->top = le16toh(block->top);
	block->bottom = le16toh(block->bottom);
	block->lid = le16toh(block->lid);
}

void fix_map_light_endian(struct map_light* light)
{
	light->x = le16toh(light->x);
	light->y = le16toh(light->y);
	light->z = le16toh(light->z);
	light->radius = le16toh(light->radius);
}

void fix_tile_animation_endian(struct tile_animation* animation)
{
	animation->base = le16toh(animation->base);
	for ( size_t i = 0; i < animation->anim_length; i++ )
		animation->tiles[i] = le16toh(animation->tiles[i]);
}

void fix_map_zone_endian(struct map_zone* zone)
{
	// Nothing to do, all 8-bit integers.
	(void) zone;
}

void fix_palette_base_endian(struct palette_base* palette_base)
{
	palette_base->tile = le16toh(palette_base->tile);
	palette_base->sprite = le16toh(palette_base->sprite);
	palette_base->car_remap = le16toh(palette_base->car_remap);
	palette_base->ped_remap = le16toh(palette_base->ped_remap);
	palette_base->code_obj_remap = le16toh(palette_base->code_obj_remap);
	palette_base->map_obj_remap = le16toh(palette_base->map_obj_remap);
	palette_base->user_remap = le16toh(palette_base->user_remap);
	palette_base->user_remap = le16toh(palette_base->user_remap);
}

void fix_sprite_base_endian(struct sprite_base* sprite_base)
{
	sprite_base->car = le16toh(sprite_base->car);
	sprite_base->ped = le16toh(sprite_base->ped);
	sprite_base->code_obj = le16toh(sprite_base->code_obj);
	sprite_base->map_obj = le16toh(sprite_base->map_obj);
	sprite_base->user = le16toh(sprite_base->user);
	sprite_base->font = le16toh(sprite_base->font);
}

void fix_sprite_entry_endian(struct sprite_entry* sprite_entry)
{
	sprite_entry->ptr = le32toh(sprite_entry->ptr);
}

void decompress_dmap(FILE* fp, const char* file_path, struct map* map)
{
	struct compressed_map_part1_dmap part1;
	if ( !fread(&part1, sizeof(part1), 1, fp) )
		error(1, errno, "`%s'", file_path);
	fix_compressed_map_part1_dmap_endian(&part1);
	uint32_t* columns = (uint32_t*) malloc(sizeof(uint32_t) * part1.column_words);
	if ( !columns )
		error(1, errno, "malloc");
	if ( fread(columns, sizeof(uint32_t), part1.column_words, fp) != part1.column_words )
		error(1, errno, "`%s'", file_path);
	for ( uint32_t i = 0; i < part1.column_words; i++ )
		columns[i] = le32toh(columns[i]);
	struct compressed_map_part2_dmap part2;
	if ( !fread(&part2, sizeof(part2), 1, fp) )
		error(1, errno, "`%s'", file_path);
	fix_compressed_map_part2_dmap_endian(&part2);
	struct block_info* blocks = (struct block_info*) malloc(sizeof(struct block_info) * part2.num_blocks);
	if ( !blocks )
		error(1, errno, "malloc");
	if ( fread(blocks, sizeof(struct block_info), part2.num_blocks, fp) != part2.num_blocks )
		error(1, errno, "`%s'", file_path);
	for ( size_t i = 0; i < part2.num_blocks; i++ )
		fix_block_info_endian(&blocks[i]);
	for ( size_t z = 0; z < MAP_HEIGHT; z++ )
		for ( size_t y = 0; y < MAP_BREADTH; y++ )
			for ( size_t x = 0; x < MAP_WIDTH; x++ )
			{
				uint32_t base = part1.base[y][x];
				struct col_info_dmap* col_info = (struct col_info_dmap*) (columns + base);
				uint32_t block_id = col_info->offset <= z && z < col_info->height
				                  ? le32toh(col_info->blockd[z - col_info->offset])
				                  : 0;
				map->city_scape[z][y][x] = blocks[block_id];
			}
	free(blocks);
	free(columns);
}

void load_zones(FILE* fp, const char* file_path, struct level* level,
                uint32_t chunk_size)
{
	uint8_t* zone_data = (uint8_t*) malloc(chunk_size);
	if ( !zone_data )
		error(1, errno, "malloc");
	if ( fread(zone_data, 1, chunk_size, fp) != chunk_size )
		error(1, errno, "`%s'", file_path);
	for ( uint32_t offset = 0; offset < chunk_size; )
	{
		struct map_zone* zone = (struct map_zone*) (zone_data + offset);
		fix_map_zone_endian(zone);
		offset += sizeof(struct map_zone) + zone->name_length;
	}
	for ( uint32_t offset = 0; offset < chunk_size; )
	{
		struct map_zone* zone = (struct map_zone*) (zone_data + offset);
		if ( zone->zone_type == ZONE_RESTART )
		{
			if ( rand() % ++level->num_spawn_zones == 0 )
			{
				level->spawn_x = zone->x;
				level->spawn_y = zone->y;
			}
		}
		offset += sizeof(struct map_zone) + zone->name_length;
	}
	free(zone_data);
}

void load_lights(FILE* fp, const char* file_path, struct level* level,
                uint32_t chunk_size)
{
	level->num_lights = chunk_size / sizeof(struct map_light);
	level->lights = (struct map_light*) malloc(chunk_size);
	if ( !level->lights )
		error(1, errno, "malloc");
	if ( fread(level->lights, 1, chunk_size, fp) != chunk_size )
		error(1, errno, "`%s'", file_path);
	for ( size_t i = 0; i < level->num_lights; i++ )
		fix_map_light_endian(&level->lights[i]);
	level->light_time_left = (uint8_t*) malloc(sizeof(uint8_t) * level->num_lights);
	if ( !level->light_time_left )
		error(1, errno, "malloc");
	level->light_on = (bool*) malloc(sizeof(bool) * level->num_lights);
	if ( !level->light_on )
		error(1, errno, "malloc");
	for ( size_t i = 0; i < level->num_lights; i++ )
	{
		if ( level->lights[i].off_time || level->lights[i].shape )
		{
			level->light_time_left[i] = level->lights[i].off_time + rand() % (level->lights[i].shape + 1);
			level->light_on[i] = false;
		}
		else
		{
			level->light_time_left[i] = 0.0f;
			level->light_on[i] = true;
		}
	}
}

void load_tile_animations(FILE* fp, const char* file_path, struct level* level,
                          uint32_t chunk_size)
{
	level->tile_animation_size = chunk_size;
	level->tile_animation_data = (uint8_t*) malloc(level->tile_animation_size);
	if ( !level->tile_animation_data )
		error(1, errno, "malloc");
	if ( fread(level->tile_animation_data, 1, chunk_size, fp) != chunk_size )
		error(1, errno, "`%s'", file_path);
	for ( size_t offset = 0; offset < level->tile_animation_size; )
	{
		struct tile_animation* animation = (struct tile_animation*) (level->tile_animation_data + offset);
		fix_tile_animation_endian(animation);
		offset += sizeof(*animation) + sizeof(animation->tiles[0]) * animation->anim_length;
	}
}

void load_level(const char* file_path, struct level* level)
{
	FILE* fp = fopen(file_path, "r");
	if ( !fp )
		error(1, errno, "`%s'", file_path);
	struct file_header file_header;
	if ( !fread(&file_header, sizeof(file_header), 1, fp) )
		error(1, errno, "`%s'", file_path);
	fix_file_header_endian(&file_header);
	if ( memcmp(file_header.file_type, MAP_FILE_HEADER_TYPE, 4) )
		error(1, 0, "`%s': Not a GTA 2 Map File", file_path);
	struct chunk_header chunk_header;
	while ( fread(&chunk_header, sizeof(chunk_header), 1, fp) )
	{
		fix_chunk_header_endian(&chunk_header);
		off_t start_pos = ftello(fp);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_ANIM, 4) )
			load_tile_animations(fp, file_path, level, chunk_header.chunk_size);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_DMAP, 4) )
			decompress_dmap(fp, file_path, &level->map);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_ZONE, 4) )
			load_zones(fp, file_path, level, chunk_header.chunk_size);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_LGHT, 4) )
			load_lights(fp, file_path, level, chunk_header.chunk_size);
		fseeko(fp, start_pos + chunk_header.chunk_size, SEEK_SET);
	}
	if ( ferror(fp) )
		error(1, errno, "`%s'", file_path);
	for ( size_t i = 0; i < 1024; i++ )
	{
		level->tile_displayed[i] = i;
		level->tile_current_frame[i] = 0;
		level->tile_frame_time[i] = 0;
		level->tile_repeats[i] = 0;
	}
	fclose(fp);
}

void load_tiles(FILE* fp, const char* file_path, struct style* style)
{
	size_t data_size = 63 * 256 * 256;
	if ( fread(style->tile_pages, sizeof(uint8_t), data_size, fp) != data_size )
		error(1, errno, "`%s'", file_path);
}

void load_palette_bases(FILE* fp, const char* file_path, struct style* style)
{
	size_t data_size = sizeof(style->palette_base_count);
	if ( fread(&style->palette_base_count, 1, data_size, fp) != data_size )
		error(1, errno, "`%s'", file_path);
	fix_palette_base_endian(&style->palette_base_count);
	uint16_t accumulator = 0;
	style->palette_base.tile = accumulator;
	accumulator += style->palette_base_count.tile;
	style->palette_base.sprite = accumulator;
	accumulator += style->palette_base_count.sprite;
	style->palette_base.car_remap = accumulator;
	accumulator += style->palette_base_count.car_remap;
	style->palette_base.ped_remap = accumulator;
	accumulator += style->palette_base_count.ped_remap;
	style->palette_base.code_obj_remap = accumulator;
	accumulator += style->palette_base_count.code_obj_remap;
	style->palette_base.map_obj_remap = accumulator;
	accumulator += style->palette_base_count.map_obj_remap;
	style->palette_base.user_remap = accumulator;
	accumulator += style->palette_base_count.user_remap;
	style->palette_base.font_remap = accumulator;
	accumulator += style->palette_base_count.font_remap;
}

void load_physical_palette(FILE* fp, const char* file_path, struct style* style,
                           uint32_t chunk_size)
{
	size_t data_size = chunk_size;
	if ( sizeof(style->palette_data) < data_size )
		data_size = sizeof(style->palette_data);
	if ( fread(style->palette_data, 1, data_size, fp) != data_size )
		error(1, errno, "`%s'", file_path);
}

void load_palette_index(FILE* fp, const char* file_path, struct style* style)
{
	size_t data_size = 16384;
	if ( fread(&style->palette_index, sizeof(uint16_t), data_size, fp) != data_size )
		error(1, errno, "`%s'", file_path);
}

void load_sprite_bases(FILE* fp, const char* file_path, struct style* style)
{
	size_t data_size = sizeof(style->sprite_base_count);
	if ( fread(&style->sprite_base_count, 1, data_size, fp) != data_size )
		error(1, errno, "`%s'", file_path);
	fix_sprite_base_endian(&style->sprite_base_count);
	uint16_t accumulator = 0;
	style->sprite_base.car = accumulator;
	accumulator += style->sprite_base_count.car;
	style->sprite_base.ped = accumulator;
	accumulator += style->sprite_base_count.ped;
	style->sprite_base.code_obj = accumulator;
	accumulator += style->sprite_base_count.code_obj;
	style->sprite_base.map_obj = accumulator;
	accumulator += style->sprite_base_count.map_obj;
	style->sprite_base.user = accumulator;
	accumulator += style->sprite_base_count.user;
	style->sprite_base.font = accumulator;
	accumulator += style->sprite_base_count.font;
}

void load_sprite_entries(FILE* fp, const char* file_path, struct style* style,
                         uint32_t chunk_size)
{
	style->sprite_entries = (struct sprite_entry*) malloc(chunk_size);
	if ( !style->sprite_entries )
		error(1, errno, "malloc");
	size_t data_size = chunk_size;
	if ( fread(style->sprite_entries, 1, data_size, fp) != data_size )
		error(1, errno, "`%s'", file_path);
	style->num_sprite_entries = chunk_size / sizeof(struct sprite_entry);
	for ( size_t i = 0; i < style->num_sprite_entries; i++ )
		fix_sprite_entry_endian(&style->sprite_entries[i]);
}

void load_sprite_data(FILE* fp, const char* file_path, struct style* style,
                      uint32_t chunk_size)
{
	style->sprite_data_size = chunk_size;
	style->sprite_data = (uint8_t*) malloc(style->sprite_data_size);
	if ( !style->sprite_data )
		error(1, errno, "malloc");
	size_t data_size = chunk_size;
	if ( fread(style->sprite_data, 1, data_size, fp) != data_size )
		error(1, errno, "`%s'", file_path);
	style->num_sprite_pages = style->sprite_data_size / SPRITE_DATA_PAGE_SIZE;
	style->sprite_pages = (GLuint*) malloc(sizeof(GLuint) * style->num_sprite_pages);
	if ( !style->sprite_pages )
		error(1, errno, "malloc");
}

void load_style(const char* file_path, struct style* style)
{
	FILE* fp = fopen(file_path, "r");
	if ( !fp )
		error(1, errno, "`%s'", file_path);
	struct file_header file_header;
	if ( !fread(&file_header, sizeof(file_header), 1, fp) )
		error(1, errno, "`%s'", file_path);
	fix_file_header_endian(&file_header);
	if ( memcmp(file_header.file_type, STYLE_FILE_HEADER_TYPE, 4) )
		error(1, 0, "`%s': Not a GTA 2 Style File", file_path);
	struct chunk_header chunk_header;
	while ( fread(&chunk_header, sizeof(chunk_header), 1, fp) )
	{
		fix_chunk_header_endian(&chunk_header);
		off_t start_pos = ftello(fp);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_PALB, 4) )
			load_palette_bases(fp, file_path, style);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_TILE, 4) )
			load_tiles(fp, file_path, style);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_PPAL, 4) )
			load_physical_palette(fp, file_path, style, chunk_header.chunk_size);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_PALX, 4) )
			load_palette_index(fp, file_path, style);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_SPRB, 4) )
			load_sprite_bases(fp, file_path, style);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_SPRG, 4) )
			load_sprite_data(fp, file_path, style, chunk_header.chunk_size);
		if ( !memcmp(chunk_header.chunk_type, CHUNK_TYPE_SPRX, 4) )
			load_sprite_entries(fp, file_path, style, chunk_header.chunk_size);
		fseeko(fp, start_pos + chunk_header.chunk_size, SEEK_SET);
	}
	if ( ferror(fp) )
		error(1, errno, "`%s'", file_path);
	fclose(fp);
}

typedef struct
{
	uint8_t r;
	uint8_t g;
	uint8_t b;
} rgb8_t;

static inline rgb8_t MakeColor(uint8_t r, uint8_t g, uint8_t b)
{
	rgb8_t ret = { r, g, b };
	return ret;
}

static inline void glColorrgb8_t(rgb8_t rgb)
{
	glColor3ub(rgb.r, rgb.g, rgb.b);
}

char* ReadTextFileContents(const char* path)
{
	FILE* fp = fopen(path, "r");
	if ( !fp )
		return NULL;
	if ( fseeko(fp, 0, SEEK_END) < 0 )
		return fclose(fp), (char*) NULL;
	off_t length = ftello(fp);
	if ( length < 0 )
		return fclose(fp), (char*) NULL;
	size_t numchars = length / sizeof(char);
	if ( fseeko(fp, 0, SEEK_SET) < 0 )
		return fclose(fp), (char*) NULL;
	char* ret = new char[length+1];
	size_t charsread = fread(ret, sizeof(char), numchars, fp);
	fclose(fp);
	if ( charsread != numchars ) { delete[] ret; return NULL; }
	ret[length] = '\0';
	return ret;
}

char* ReadTextFileContentsError(const char* path)
{
	char* ret = ReadTextFileContents(path);
	if ( ret )
		return ret;
	error(0, errno, "%s", path);
	return NULL;
}

void PrintShaderInfoLog(GLenum shader, bool compiled, const char* path)
{
	int length = 0;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &length);
	if ( 0 < length )
	{
		int charswritten = 0;
		char* log = new char[length+1];
		glGetShaderInfoLog(shader, length, &charswritten, log);
		if ( 0 < charswritten )
			fprintf(stderr, "Shader InfoLog: %s\n%s", path, log);
		delete[] log;
	}
	if ( !compiled )
		fprintf(stderr, "Unable to compile shader: %s", path);
}

void PrintProgramInfoLog(GLenum program, bool linked)
{
	int length = 0;
	glGetProgramiv(program, GL_INFO_LOG_LENGTH, &length);
	if ( 0 < length )
	{
		int charswritten = 0;
		char* log = new char[length+1];
		glGetProgramInfoLog(program, length, &charswritten, log);
		if ( 0 < charswritten )
			fprintf(stderr, "Shader InfoLog:\n%s", log);
		delete[] log;
	}
	if ( !linked )
		fprintf(stderr, "Unable to link shader program\n");
}

bool CompileShader(GLenum* out, GLenum type, const char* src, const char* path)
{
	GLenum shader = glCreateShaderObjectARB(type);
	glShaderSourceARB(shader, 1, &src, NULL);
	glCompileShaderARB(shader);
	GLint compiled;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &compiled);
	PrintShaderInfoLog(shader, compiled != 0, path);
	if ( !compiled )
	{
		glDeleteObjectARB(shader);
		return false;
	}
	*out = shader;
	return true;
}

bool CompileShaderFromPath(GLenum* out, GLenum type, const char* path)
{
	char* src = ReadTextFileContentsError(path);
	if ( !src )
		return false;
	bool ret = CompileShader(out, type, src, path);
	delete[] src;
	return ret;
}

bool LinkProgram(GLenum pipeline_program)
{
	glLinkProgramARB(pipeline_program);
	GLint linked;
	glGetProgramiv(pipeline_program, GL_LINK_STATUS, &linked);
	PrintProgramInfoLog(pipeline_program, linked != 0);
	return linked != 0;
}

const int DEFAULT_WINDOW_WIDTH = 1280;
const int DEFAULT_WINDOW_HEIGHT = 720;

int screen_width = -1;
int screen_height = -1;
int screen_bpp = -1;
uint32_t sdl_video_flags;
SDL_Surface* sdl_screen;

struct level level;
struct style style;

float camera_x = 0;
float camera_y = 0;
float camera_z = 10; /* low = 10, top = 16*/
float camera_fov = 30; /* normal = 30 */

struct car
{
	uint16_t model;
	float x;
	float y;
	float z;
	float angle;
	float speed;
};

struct car_control
{
	bool forward;
	bool left;
	bool right;
	bool backward;
};

struct car car = { 76, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f };

bool is_driving = false;

bool key_left = false;
bool key_right = false;
bool key_up = false;
bool key_down = false;

GLenum phong_vertex;
GLenum phong_frag;
GLenum pipeline_program;

bool InitializeSimulation()
{
	for ( size_t i = 0; i < 64; i++ )
	{
		for ( size_t row = 0; row < 256; row++ )
			for ( size_t palette = 0; palette < 64; palette++ )
				if ( !row )
					style.palette_data[i][row*64 + palette] &= htole32(0x00FFFFFF);
				else
					style.palette_data[i][row*64 + palette] |= htole32(0xFF000000);
	}

	glGenTextures(64, style.palette_pages);
	for ( size_t i = 0; i < 64; i++ )
	{
		glBindTexture(GL_TEXTURE_2D, style.palette_pages[i]);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, 64, 256, 0, GL_BGRA, GL_UNSIGNED_BYTE, style.palette_data[i]);
	}

	glGenTextures(1024, style.tiles);
	for ( size_t i = 0; i < 1024; i++ )
	{
		const size_t width = 64;
		const size_t height = 64;
		uint8_t buffer[width * height];
		uint16_t tile_page_id = i / 16;
		uint16_t tile_page_off = i % 16;
		uint16_t tile_grid_x = tile_page_off % 4 * 64;
		uint16_t tile_grid_y = tile_page_off / 4 * 64;
		uint8_t* tile_page = style.tile_pages[tile_page_id];
		for ( size_t y = 0; y < width; y++ )
			for ( size_t x = 0; x < width; x++ )
			{
				uint8_t color = tile_page[(tile_grid_y + y) * 256 + (tile_grid_x + x)];
				buffer[y*width+x] = color;
			}

		glBindTexture(GL_TEXTURE_2D, style.tiles[i]);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, width, height, 0, GL_RED, GL_UNSIGNED_BYTE, buffer);
	}

	glGenTextures(style.num_sprite_pages, style.sprite_pages);
	for ( size_t i = 0; i < style.num_sprite_pages; i++ )
	{
		glBindTexture(GL_TEXTURE_2D, style.sprite_pages[i]);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB8, 256, 256, 0, GL_RED, GL_UNSIGNED_BYTE, style.sprite_data + 256 * 256 * sizeof(uint8_t) * i);
	}

	const char* phong_vertex_path = "phong-vertex.glsl";
	const char* phong_frag_path = "phong-frag.glsl";
	if ( !CompileShaderFromPath(&phong_vertex, GL_VERTEX_SHADER, phong_vertex_path) )
		return false;
	if ( !CompileShaderFromPath(&phong_frag, GL_FRAGMENT_SHADER, phong_frag_path) )
		return false;
	pipeline_program = glCreateProgramObjectARB();
	glAttachObjectARB(pipeline_program, phong_vertex);
	glAttachObjectARB(pipeline_program, phong_frag);
	if ( !LinkProgram(pipeline_program) )
		return false;

	return true;
}

void EnforceMapBoundariesOnCar(struct car* car)
{
	if ( car->x < 1.0f )
		car->x = 1.0f;
	if ( MAP_WIDTH-1 < car->x )
		car->x = MAP_WIDTH-1;
	if ( car->y < 1.0f )
		car->y = 1.0f;
	if ( MAP_BREADTH-1 < car->y )
		car->y = MAP_BREADTH-1;
}

const float MAX_FORWARD_SPEED = 8.0f;
const float MAX_BACKWARD_SPEED = 1.5f;
const float FORWARD_ACCELERATION = 1.0f;
const float BACKWARD_ACCELERATION = 0.8f;
const float FRICTION = 0.5f;
const float TURN_FACTOR = 2 * M_PI / 2.f;

void UpdateCar(struct car* car, struct car_control* control, float deltatime)
{
	if ( control->forward && !control->backward )
		car->speed += FORWARD_ACCELERATION * deltatime;
	else if ( control->backward && !control->forward && 0.5 < car->speed )
		car->speed -= 1.0 * car->speed * deltatime;
	else if ( control->backward && !control->forward && car->speed <= 0.5 )
		car->speed -= BACKWARD_ACCELERATION * deltatime;
	else
		car->speed -= FRICTION * car->speed * deltatime;

	if ( MAX_FORWARD_SPEED < car->speed )
		car->speed = MAX_FORWARD_SPEED;
	if ( car->speed < -MAX_BACKWARD_SPEED )
		car->speed = -MAX_BACKWARD_SPEED;

	float turn_factor = car->speed;
	if ( 1.0f < turn_factor )
		turn_factor = 1.0f;
	if ( control->left && !control->right )
		car->angle += TURN_FACTOR * turn_factor * deltatime;
	if ( control->right && !control->left )
		car->angle -= TURN_FACTOR * turn_factor * deltatime;
	car->x += cos(car->angle) * car->speed * deltatime;
	car->y += -sin(car->angle) * car->speed * deltatime;

	EnforceMapBoundariesOnCar(car);
}

const float WALK_SPEED = 3.0f;

void UpdateWalkingCar(struct car* car, struct car_control* control, float deltatime)
{
	if ( control->left && !control->right )
		car->x -= WALK_SPEED * deltatime;
	if ( control->right && !control->left )
		car->x += WALK_SPEED * deltatime;
	if ( control->forward && !control->backward )
		car->y -= WALK_SPEED * deltatime;
	if ( control->backward && !control->forward )
		car->y += WALK_SPEED * deltatime;

	EnforceMapBoundariesOnCar(car);
}

void UpdateSimulation(float deltatime)
{
	struct car_control player_car_control;
	player_car_control.forward = key_up;
	player_car_control.backward = key_down;
	player_car_control.left = key_left;
	player_car_control.right = key_right;

	if ( is_driving )
		UpdateCar(&car, &player_car_control, deltatime);
	else
		UpdateWalkingCar(&car, &player_car_control, deltatime);

	for ( size_t h = MAP_HEIGHT; h; h-- )
	{
		struct block_info* block = &level.map.city_scape[h-1][(size_t) car.y][(size_t) (car.x)];
		if ( (block->slope_type >> GROUND_TYPE_SHIFT & GROUND_TYPE_MASK) == GROUND_TYPE_AIR )
			continue;
		if ( !block->lid )
			continue;
		car.z = h;
		break;
	}
	EnforceMapBoundariesOnCar(&car);

	for ( size_t offset = 0; offset < level.tile_animation_size; )
	{
		struct tile_animation* animation = (struct tile_animation*) (level.tile_animation_data + offset);
		offset += sizeof(*animation) + sizeof(animation->tiles[0]) * animation->anim_length;
		if ( animation->repeat && animation->repeat == level.tile_repeats[animation->base] )
			continue;
		if ( ++level.tile_frame_time[animation->base] == animation->frame_rate )
		{
			level.tile_frame_time[animation->base] = 0;
			if ( ++level.tile_current_frame[animation->base] == animation->anim_length )
			{
				level.tile_current_frame[animation->base] = 0;
				if ( ++level.tile_repeats[animation->base] == animation->repeat && animation->repeat )
					continue;
			}
		}
		level.tile_displayed[animation->base] = animation->tiles[level.tile_current_frame[animation->base]];
	}

	for ( size_t i = 0; i < level.num_lights; i++ )
	{
		if ( !(level.lights[i].off_time || level.lights[i].shape) )
			continue;
		if ( !level.light_time_left[i] || !--level.light_time_left[i] )
		{
			if ( (level.light_on[i] = !level.light_on[i]) )
				level.light_time_left[i] = level.lights[i].on_time + rand() % (level.lights[i].shape + 1);
			else
				level.light_time_left[i] = level.lights[i].off_time + rand() % (level.lights[i].shape + 1);
		}
	}

	camera_x = car.x;
	camera_y = car.y;
	camera_z = (car.speed / MAX_FORWARD_SPEED) * 6.0f + 10.0f + (car.z/8.0) * 2.5f;
}

static void PrepareProjection()
{
	if( !screen_height )
		screen_height = 1;

	float screen_ratio = (float) screen_width / (float) screen_height;

	// Set the background color on clearing.
	glClearColor(0.05f, 0.05f, 0.5f, 0.0f);

	// Reset the coordinate system before modifying
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	// Set the viewport to be the entire window
	glViewport(0, 0, screen_width, screen_height);

	// Set the correct perspective.
	gluPerspective(camera_fov, screen_ratio, 0.01f, 100.0f);

	glDepthFunc(GL_LEQUAL);
	glEnable(GL_DEPTH_TEST);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glEnable(GL_BLEND);
	glEnable(GL_TEXTURE_2D);
}

void RenderSimulation()
{
	PrepareProjection();

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// Use our custom phong vertex and fragment shaders.
	glUseProgramObjectARB(pipeline_program);

	// Select the matrix for transformations and reset it.
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	glEnable(GL_LIGHTING);
	glShadeModel(GL_SMOOTH);
	glEnable(GL_LIGHT0);

	// Set material properties of surfaces.
	float mat_ambient[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float mat_specular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float mat_diffuse[] = { 1.0f, 1.0f, 1.0f, 1.0f };
	float mat_emission[] = { 0.0f, 0.0f, 0.0f, 0.0f };
	float mat_shininess[] = { 100.0f, 100.0f, 100.0f, 100.0f };

	glMaterialfv(GL_FRONT, GL_AMBIENT, mat_ambient);
	glMaterialfv(GL_FRONT, GL_SPECULAR, mat_specular);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, mat_diffuse);
	glMaterialfv(GL_FRONT, GL_EMISSION, mat_emission);
	glMaterialfv(GL_FRONT, GL_SHININESS, mat_shininess);

	f4x4 camera_transform = f4x4_translate(camera_x, camera_y * -1.0f, camera_z);
	f4x4 camera_inverse; f4x4_invert(camera_transform, &camera_inverse);
	glPushMatrix();
	glMultMatrixf(camera_inverse.e);

	float nul_vec4[]  = { 0.0f, 0.0f, 0.0f, 1.0f };
	for ( size_t i = 0; i < NUM_LIGHTS; i++ )
	{
		glLightfv(GL_LIGHT0 + i, GL_AMBIENT, nul_vec4);
		glLightfv(GL_LIGHT0 + i, GL_DIFFUSE, nul_vec4);
		glLightfv(GL_LIGHT0 + i, GL_SPECULAR, nul_vec4);
		glLightfv(GL_LIGHT0 + i, GL_POSITION, nul_vec4);
	}

	GLint loc_frame_to_canonical = glGetUniformLocation(pipeline_program, "frame_to_canonical");
	GLint loc_light_source_position = glGetUniformLocation(pipeline_program, "light_source_position");
	GLint loc_light_source_diffuse = glGetUniformLocation(pipeline_program, "light_source_diffuse");
	GLint loc_light_source_radius = glGetUniformLocation(pipeline_program, "light_source_radius");

	struct map_light* best_lights[NUM_LIGHTS];
	for ( size_t i = 0; i < NUM_LIGHTS; i++ )
	{
		struct map_light* best_light = NULL;
		float best_light_contribution;
		for ( size_t j = 0; j < level.num_lights; j++ )
		{
			struct map_light* light = &level.lights[j];
			if ( !level.light_on[j] )
				continue;
			bool already_found = false;
			for ( size_t n = 0; n < i; n++ )
				if ( best_lights[n] == light )
				{
					already_found = true;
					break;
				}
			if ( already_found )
				continue;
			float dist_sq = (float_of_fix16(light->x) - camera_x) * (float_of_fix16(light->x) - camera_x) +
							(float_of_fix16(light->y) - camera_y) * (float_of_fix16(light->y) - camera_y) +
							(float_of_fix16(light->z) - camera_z) * (float_of_fix16(light->z) - camera_z);
			float dist = sqrt(dist_sq);
			float color_strength = (light->r + light->g + light->b) / (float) (3*255);
			float contribution = light->intensity * color_strength * float_of_fix16(light->radius) / dist;
			contribution = -dist_sq;
			if ( best_light && (best_light_contribution > contribution) )
				continue;
			best_light = light;
			best_light_contribution = contribution;
		}
		best_lights[i] = best_light;
	}

	float light_diffuses[NUM_LIGHTS*4];
	float light_positions[NUM_LIGHTS*4];
	float light_radia[NUM_LIGHTS];

	for ( size_t i = 0; i < NUM_LIGHTS; i++ )
	{
		if ( !best_lights[i] )
			break;
		struct map_light* best_light = best_lights[i];

		float intensity = best_light->intensity / 255.0f;
		float r = best_light->r / 255.0f * intensity;
		float g = best_light->g / 255.0f * intensity;
		float b = best_light->b / 255.0f * intensity;

		light_diffuses[i*4 + 0] = r;
		light_diffuses[i*4 + 1] = g;
		light_diffuses[i*4 + 2] = b;
		light_diffuses[i*4 + 3] = 1.0f;
		light_positions[i*4 + 0] = float_of_fix16(best_light->x);
		light_positions[i*4 + 1] = float_of_fix16(best_light->y);
		light_positions[i*4 + 2] = float_of_fix16(best_light->z);
		light_positions[i*4 + 3] = 1.0f;
		light_radia[i] = float_of_fix16(best_light->radius);
	}

	glUniform4fv(loc_light_source_position, NUM_LIGHTS, light_positions);
	glUniform4fv(loc_light_source_diffuse, NUM_LIGHTS, light_diffuses);
	glUniform1fv(loc_light_source_radius, NUM_LIGHTS, light_radia);

	glUniform1i(glGetUniformLocation(pipeline_program, "texture"), 0);
	glUniform1i(glGetUniformLocation(pipeline_program, "palette"), 1);

	glColorrgb8_t(MakeColor(255, 255, 255));

	glPushMatrix();
	glMultMatrixf(f4x4_scale(1.0f, -1.0f, 1.0f).e);
	long viewport_half_width = 16;
	long viewport_half_breadth = 16;
	for ( size_t z = 0; z < MAP_HEIGHT; z++ )
	{
		float pos_z = z;
		for ( long y = camera_y - viewport_half_breadth; y < camera_y + viewport_half_breadth; y++ )
		{
			float pos_y = y;
			size_t cy = y < 0 ? 0 : MAP_BREADTH-1 < (size_t) y ? MAP_BREADTH-1 : y;
			for ( long x = camera_x - viewport_half_width; x < camera_x + viewport_half_width; x++ )
			{
				float pos_x = x;
				size_t cx = x < 0 ? 0 : MAP_WIDTH-1 < (size_t) x ? MAP_WIDTH-1 : x;

				struct block_info* block = &level.map.city_scape[z][cy][cx];

				glPushMatrix();

				f4x4 translate = f4x4_translate(pos_x, pos_y, pos_z);
				glMultMatrixf(translate.e);
				f4x4 translate_inv;
				f4x4_invert(translate, &translate_inv);
				glUniformMatrix4fv(loc_frame_to_canonical, 1, false, translate.e);

				uint16_t left_tile = (block->left & WALL_TEXTURE_MASK) >> WALL_TEXTURE_SHIFT;
				uint16_t right_tile = (block->right & WALL_TEXTURE_MASK) >> WALL_TEXTURE_SHIFT;
				uint16_t top_tile = (block->top & WALL_TEXTURE_MASK) >> WALL_TEXTURE_SHIFT;
				uint16_t bottom_tile = (block->bottom & WALL_TEXTURE_MASK) >> WALL_TEXTURE_SHIFT;
				uint16_t lid_tile = (block->lid & WALL_TEXTURE_MASK) >> WALL_TEXTURE_SHIFT;

				left_tile = level.tile_displayed[left_tile];
				right_tile = level.tile_displayed[right_tile];
				top_tile = level.tile_displayed[top_tile];
				bottom_tile = level.tile_displayed[bottom_tile];
				lid_tile = level.tile_displayed[lid_tile];

				uint16_t left_rotate = (block->left & WALL_ROTATE_MASK) >> WALL_ROTATE_SHIFT;
				uint16_t right_rotate = (block->right & WALL_ROTATE_MASK) >> WALL_ROTATE_SHIFT;
				uint16_t top_rotate = (block->top & WALL_ROTATE_MASK) >> WALL_ROTATE_SHIFT;
				uint16_t bottom_rotate = (block->bottom & WALL_ROTATE_MASK) >> WALL_ROTATE_SHIFT;
				uint16_t lid_rotate = (block->lid & WALL_ROTATE_MASK) >> WALL_ROTATE_SHIFT;

				bool left_flip = block->left & WALL_FLIP;
				bool right_flip = block->right & WALL_FLIP;
				bool top_flip = block->top & WALL_FLIP;
				bool bottom_flip = block->bottom & WALL_FLIP;
				bool lid_flip = block->lid & WALL_FLIP;

				bool left_flat = block->left & WALL_FLAT;
				bool right_flat = block->right & WALL_FLAT;
				bool top_flat = block->top & WALL_FLAT;
				bool bottom_flat = block->bottom & WALL_FLAT;
				bool lid_flat = block->lid & WALL_FLAT;

				// TODO: This actually means that the opposite side is moved to
				//       the current flat side and then drawn as the backside.
				if ( left_flat && !right_flat )
					right_tile = 0;
				if ( right_flat && !left_flat )
					left_tile = 0;
				if ( top_flat && !bottom_flat )
					bottom_tile = 0;
				if ( bottom_flat && !top_flat )
					top_tile = 0;

				uint8_t slope_type = (block->slope_type & SLOPE_TYPE_MASK) >> SLOPE_TYPE_SHIFT;
				(void) slope_type;

				float x00 = 0.0f;
				float x01 = 0.0f;
				float x10 = 1.0f;
				float x11 = 1.0f;
				float y00 = 0.0f;
				float y10 = 0.0f;
				float y01 = 1.0f;
				float y11 = 1.0f;
				float h00 = 1.0f;
				float h10 = 1.0f;
				float h01 = 1.0f;
				float h11 = 1.0f;

				// TODO: Handle slope sides!
				if ( 1 <= slope_type && slope_type <= 2 ) /* up 26 */
				{
					h00 = 1.0f * (slope_type-0-0)/2.;
					h10 = 1.0f * (slope_type-0-0)/2.;
					h01 = 1.0f * (slope_type-0-1)/2.;
					h11 = 1.0f * (slope_type-0-1)/2.;
				}
				if ( 3 <= slope_type && slope_type <= 4 ) /* down 26 */
				{
					h01 = 1.0f * (slope_type-2-0)/2.;
					h11 = 1.0f * (slope_type-2-0)/2.;
					h00 = 1.0f * (slope_type-2-1)/2.;
					h10 = 1.0f * (slope_type-2-1)/2.;
				}
				if ( 5 <= slope_type && slope_type <= 6 ) /* right 26 */
				{
					h00 = 1.0f * (slope_type-4-0)/2.;
					h01 = 1.0f * (slope_type-4-0)/2.;
					h10 = 1.0f * (slope_type-4-1)/2.;
					h11 = 1.0f * (slope_type-4-1)/2.;
				}
				if ( 7 <= slope_type && slope_type <= 8 ) /* left 26 */
				{
					h10 = 1.0f * (slope_type-6-0)/2.;
					h11 = 1.0f * (slope_type-6-0)/2.;
					h00 = 1.0f * (slope_type-6-1)/2.;
					h01 = 1.0f * (slope_type-6-1)/2.;
				}
				if ( 9 <= slope_type && slope_type <= 16 ) /* up 7 */
				{
					h00 = 1.0f * (slope_type-8-0)/8.;
					h10 = 1.0f * (slope_type-8-0)/8.;
					h01 = 1.0f * (slope_type-8-1)/8.;
					h11 = 1.0f * (slope_type-8-1)/8.;
				}
				if ( 17 <= slope_type && slope_type <= 24 ) /* down 7 */
				{
					h01 = 1.0f * (slope_type-16-0)/8.;
					h11 = 1.0f * (slope_type-16-0)/8.;
					h00 = 1.0f * (slope_type-16-1)/8.;
					h10 = 1.0f * (slope_type-16-1)/8.;
				}
				if ( 25 <= slope_type && slope_type <= 32 ) /* right 7 */
				{
					h00 = 1.0f * (slope_type-24-0)/8.;
					h01 = 1.0f * (slope_type-24-0)/8.;
					h10 = 1.0f * (slope_type-24-1)/8.;
					h11 = 1.0f * (slope_type-24-1)/8.;
				}
				if ( 33 <= slope_type && slope_type <= 40 ) /* left 7 */
				{
					h10 = 1.0f * (slope_type-32-0)/8.;
					h11 = 1.0f * (slope_type-32-0)/8.;
					h00 = 1.0f * (slope_type-32-1)/8.;
					h01 = 1.0f * (slope_type-32-1)/8.;
				}
				if ( slope_type == 41 ) /* up 45 */
				{
					h00 = 1.0f * 1;
					h10 = 1.0f * 1;
					h01 = 1.0f * 0;
					h11 = 1.0f * 0;
				}
				if ( slope_type == 42 ) /* down 45*/
				{
					h01 = 1.0f * 1;
					h11 = 1.0f * 1;
					h00 = 1.0f * 0;
					h10 = 1.0f * 0;
				}
				if ( slope_type == 43 ) /* right  45*/
				{
					h00 = 1.0f * 1;
					h01 = 1.0f * 1;
					h10 = 1.0f * 0;
					h11 = 1.0f * 0;
				}
				if ( slope_type == 44 ) /* left 45 */
				{
					h10 = 1.0f * 1;
					h11 = 1.0f * 1;
					h00 = 1.0f * 0;
					h01 = 1.0f * 0;
				}
				// TODO: Crop rather than scale texture!
				if ( slope_type == 45 ) /* diagional, facing up left */
				{
					x00 = x10;
					y00 = y10;
				}
				if ( slope_type == 46 ) /* diagional, facing up right */
				{
					x10 = x00;
					y10 = y00;
				}
				if ( slope_type == 47 ) /* diagional, facing down left */
				{
					x01 = x11;
					y01 = y11;
				}
				if ( slope_type == 48 ) /* diagional, facing down right */
				{
					x11 = x01;
					y11 = y01;
				}
				if ( slope_type == 49 && lid_tile == 1023 ) /* 3-sided diagional slope, facing up left */
				{
					x00 = x10;
					y00 = y10;
					h00 = h01 = h10 = 0.0f;
					lid_tile = left_tile;
				}
				if ( slope_type == 49 && lid_tile != 1023 ) /* 4-sided diagional slope, facing up left */
				{
					h00 = 0.0f;
				}
				if ( slope_type == 50 && lid_tile == 1023 ) /* 3-sided diagional slope, facing up right */
				{
					x10 = x00;
					y10 = y00;
					h00 = h10 = h11 = 0.0f;
					lid_tile = right_tile;
				}
				if ( slope_type == 50 && lid_tile != 1023 ) /* 4-sided diagional slope, facing up right */
				{
					h10 = 0.0f;
				}
				if ( slope_type == 51 && lid_tile == 1023 ) /* 3-sided diagional slope, facing down left */
				{
					x01 = x11;
					y01 = y11;
					h00 = h01 = h11 = 0.0f;
					lid_tile = left_tile;
				}
				if ( slope_type == 51 && lid_tile != 1023 ) /* 4-sided diagional slope, facing down left */
				{
					h01 = 0.0f;
				}
				if ( slope_type == 52 && lid_tile == 1023 ) /* 3-sided diagional slope, facing down right */
				{
					x11 = x01;
					y11 = y01;
					h01 = h10 = h11 = 0.0f;
					lid_tile = right_tile;
				}
				if ( slope_type == 52 && lid_tile != 1023 ) /* 4-sided diagional slope, facing down right */
				{
					h11 = 0.0f;
				}
				if ( slope_type == 53 ) /* partial block left */
				{
					x10 = 24.0f/64.0f;
					x11 = 24.0f/64.0f;
				}
				if ( slope_type == 54 ) /* partial block right */
				{
					x00 = (64.0f - 24.0f) / 64.0f;
					x01 = (64.0f - 24.0f) / 64.0f;
				}
				if ( slope_type == 55 ) /* partial block top */
				{
					y11 = 24.0f/64.0f;
					y01 = 24.0f/64.0f;
				}
				if ( slope_type == 56 ) /* partial block bottom */
				{
					y00 = (64.0f - 24.0f) / 64.0f;
					y10 = (64.0f - 24.0f) / 64.0f;
				}
				if ( slope_type == 57 ) /* partial block top left corner */
				{
					x10 = 24.0f/64.0f;
					x11 = 24.0f/64.0f;
					y11 = 24.0f/64.0f;
					y01 = 24.0f/64.0f;
				}
				if ( slope_type == 58 ) /* partial block top right corner */
				{
					x00 = (64.0f - 24.0f) / 64.0f;
					x01 = (64.0f - 24.0f) / 64.0f;
					y01 = 24.0f/64.0f;
					y11 = 24.0f/64.0f;
				}
				if ( slope_type == 59 ) /* partial block bottom right corner */
				{
					y10 = (64.0f - 24.0f) / 64.0f;
					x00 = (64.0f - 24.0f) / 64.0f;
					y00 = (64.0f - 24.0f) / 64.0f;
					x01 = (64.0f - 24.0f) / 64.0f;
				}
				if ( slope_type == 60 ) /* partial block bottom left corner */
				{
					y00 = (64.0f - 24.0f) / 64.0f;
					x10 = 24.0f/64.0f;
					y10 = (64.0f - 24.0f) / 64.0f;
					x11 = 24.0f/64.0f;
				}
				if ( slope_type == 61 ) /* partial centre block 16x16 */
				{
					x00 = (32.0f - 16.0f/2.0f) / 64.0f;
					y00 = (32.0f - 16.0f/2.0f) / 64.0f;
					x01 = (32.0f - 16.0f/2.0f) / 64.0f;
					y01 = (32.0f + 16.0f/2.0f) / 64.0f;
					x10 = (32.0f + 16.0f/2.0f) / 64.0f;
					y10 = (32.0f - 16.0f/2.0f) / 64.0f;
					x11 = (32.0f + 16.0f/2.0f) / 64.0f;
					y11 = (32.0f + 16.0f/2.0f) / 64.0f;
				}

				#define make_texcords(is_flipped) \
					{ { ((is_flipped) ? 0.0f : 1.0f), 1.0f }, \
					  { ((is_flipped) ? 1.0f : 0.0f), 1.0f }, \
					  { ((is_flipped) ? 1.0f : 0.0f), 0.0f }, \
					  { ((is_flipped) ? 0.0f : 1.0f), 0.0f }, }

				// Top
				if ( top_tile )
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, style.tiles[top_tile % 1024]);
					glActiveTexture(GL_TEXTURE1);
					uint16_t palette_id = style.palette_index.phys_palette[top_tile % 1024];
					glBindTexture(GL_TEXTURE_2D, style.palette_pages[palette_id / 64]);
					glUniform1f(glGetUniformLocation(pipeline_program, "palette_column"), (palette_id % 64 + 0.5f) / 64.0f);
					glUniform1f(glGetUniformLocation(pipeline_program, "min_alpha"), top_flat ? 0.0f : 1.0f);

					if ( top_flip && top_rotate % 2 == 1 )
						top_rotate = (top_rotate + 2) % 4;
					glBegin(GL_QUADS);
					const float texcords[4][2] = make_texcords(top_flip);
					glNormal3f(0.0f, 1.0f, 0.0f);
					glTexCoord2f(texcords[(4 - top_rotate) % 4][0], texcords[(4 - top_rotate) % 4][1]);
					glVertex3f(x10, y10, 0.0f);
					glTexCoord2f(texcords[(5 - top_rotate) % 4][0], texcords[(5 - top_rotate) % 4][1]);
					glVertex3f(x00, y00, 0.0f);
					glTexCoord2f(texcords[(6 - top_rotate) % 4][0], texcords[(6 - top_rotate) % 4][1]);
					glVertex3f(x00, y00, h00);
					glTexCoord2f(texcords[(7 - top_rotate) % 4][0], texcords[(7 - top_rotate) % 4][1]);
					glVertex3f(x10, y10, h10);
					glEnd();
				}

				// Bottom
				if ( bottom_tile )
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, style.tiles[bottom_tile % 1024]);
					glActiveTexture(GL_TEXTURE1);
					uint16_t palette_id = style.palette_index.phys_palette[bottom_tile % 1024];
					glBindTexture(GL_TEXTURE_2D, style.palette_pages[palette_id / 64]);
					glUniform1f(glGetUniformLocation(pipeline_program, "palette_column"), (palette_id % 64 + 0.5f) / 64.0f);
					glUniform1f(glGetUniformLocation(pipeline_program, "min_alpha"), bottom_flat ? 0.0f : 1.0f);

					if ( bottom_flip && bottom_rotate % 2 == 1 )
						bottom_rotate = (bottom_rotate + 2) % 4;
					glBegin(GL_QUADS);
					const float texcords[4][2] = make_texcords(bottom_flip);
					glNormal3f(0.0f, -1.0f, 0.0f);
					glTexCoord2f(texcords[(4 - bottom_rotate) % 4][0], texcords[(4 - bottom_rotate) % 4][1]);
					glVertex3f(x11, y11, 0.0f);
					glTexCoord2f(texcords[(5 - bottom_rotate) % 4][0], texcords[(5 - bottom_rotate) % 4][1]);
					glVertex3f(x01, y01, 0.0f);
					glTexCoord2f(texcords[(6 - bottom_rotate) % 4][0], texcords[(6 - bottom_rotate) % 4][1]);
					glVertex3f(x01, y01, h01);
					glTexCoord2f(texcords[(7 - bottom_rotate) % 4][0], texcords[(7 - bottom_rotate) % 4][1]);
					glVertex3f(x11, y11, h11);
					glEnd();
				}

				// Left
				if ( left_tile )
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, style.tiles[left_tile % 1024]);
					glActiveTexture(GL_TEXTURE1);
					uint16_t palette_id = style.palette_index.phys_palette[left_tile % 1024];
					glBindTexture(GL_TEXTURE_2D, style.palette_pages[palette_id / 64]);
					glUniform1f(glGetUniformLocation(pipeline_program, "palette_column"), (palette_id % 64 + 0.5f) / 64.0f);
					glUniform1f(glGetUniformLocation(pipeline_program, "min_alpha"), left_flat ? 0.0f : 1.0f);

					if ( left_flip && left_rotate % 2 == 1 )
						left_rotate = (left_rotate + 2) % 4;
					glBegin(GL_QUADS);
					const float texcords[4][2] = make_texcords(left_flip);
					glNormal3f(-1.0f, 0.0f, 0.0f);
					glTexCoord2f(texcords[(4 - left_rotate) % 4][0], texcords[(4 - left_rotate) % 4][1]);
					glVertex3f(x01, y01, 0.0f);
					glTexCoord2f(texcords[(5 - left_rotate) % 4][0], texcords[(5 - left_rotate) % 4][1]);
					glVertex3f(x00, y00, 0.0f);
					glTexCoord2f(texcords[(6 - left_rotate) % 4][0], texcords[(6 - left_rotate) % 4][1]);
					glVertex3f(x00, y00, h00);
					glTexCoord2f(texcords[(7 - left_rotate) % 4][0], texcords[(7 - left_rotate) % 4][1]);
					glVertex3f(x01, y01, h01);
					glEnd();
				}

				// Right
				if ( right_tile )
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, style.tiles[right_tile % 1024]);
					glActiveTexture(GL_TEXTURE1);
					uint16_t palette_id = style.palette_index.phys_palette[right_tile % 1024];
					glBindTexture(GL_TEXTURE_2D, style.palette_pages[palette_id / 64]);
					glUniform1f(glGetUniformLocation(pipeline_program, "palette_column"), (palette_id % 64 + 0.5f) / 64.0f);
					glUniform1f(glGetUniformLocation(pipeline_program, "min_alpha"), right_flat ? 0.0f : 1.0f);

					if ( right_flip && right_rotate % 2 == 1 )
						right_rotate = (right_rotate + 2) % 4;
					glBegin(GL_QUADS);
					const float texcords[4][2] = make_texcords(right_flip);
					glNormal3f(1.0f, 0.0f, 0.0f);
					glTexCoord2f(texcords[(4 - right_rotate) % 4][0], texcords[(4 - right_rotate) % 4][1]);
					glVertex3f(x10, y10, 0.0f);
					glTexCoord2f(texcords[(5 - right_rotate) % 4][0], texcords[(5 - right_rotate) % 4][1]);
					glVertex3f(x11, y11, 0.0f);
					glTexCoord2f(texcords[(6 - right_rotate) % 4][0], texcords[(6 - right_rotate) % 4][1]);
					glVertex3f(x11, y11, h11);
					glTexCoord2f(texcords[(7 - right_rotate) % 4][0], texcords[(7 - right_rotate) % 4][1]);
					glVertex3f(x10, y10, h10);
					glEnd();
				}

				// Lid
				if ( lid_tile )
				{
					glActiveTexture(GL_TEXTURE0);
					glBindTexture(GL_TEXTURE_2D, style.tiles[lid_tile % 1024]);
					glActiveTexture(GL_TEXTURE1);
					uint16_t palette_id = style.palette_index.phys_palette[lid_tile % 1024];
					glBindTexture(GL_TEXTURE_2D, style.palette_pages[palette_id / 64]);
					glUniform1f(glGetUniformLocation(pipeline_program, "palette_column"), (palette_id % 64 + 0.5f) / 64.0f);
					glUniform1f(glGetUniformLocation(pipeline_program, "min_alpha"), lid_flat ? 0.0f : 1.0f);

					if ( lid_flip && lid_rotate % 2 == 1 )
						lid_rotate = (lid_rotate + 2) % 4;
					glBegin(GL_QUADS);
					const float texcords[4][2] = make_texcords(lid_flip);
					glNormal3f(0.0f, 0.0f, 1.0f);
					glTexCoord2f(texcords[(4 - lid_rotate) % 4][0], texcords[(4 - lid_rotate) % 4][1]);
					glVertex3f(x11, y11, h11);
					glTexCoord2f(texcords[(5 - lid_rotate) % 4][0], texcords[(5 - lid_rotate) % 4][1]);
					glVertex3f(x01, y01, h01);
					glTexCoord2f(texcords[(6 - lid_rotate) % 4][0], texcords[(6 - lid_rotate) % 4][1]);
					glVertex3f(x00, y00, h00);
					glTexCoord2f(texcords[(7 - lid_rotate) % 4][0], texcords[(7 - lid_rotate) % 4][1]);
					glVertex3f(x10, y10, h10);
					glEnd();
				}

				glPopMatrix();
			}
		}

		if ( is_driving && z <= floor(car.z) && ceil(car.z) < z+1 )
		{
			uint16_t car_sprite = car.model % style.sprite_base_count.car;
			uint16_t sprite_id = style.sprite_base.car + car_sprite;

			struct sprite_entry* sprite = &style.sprite_entries[sprite_id];
			size_t sprite_page_size = 256 * 256 * sizeof(uint8_t);
			size_t sprite_page = sprite->ptr / sprite_page_size;
			size_t sprite_page_off = sprite->ptr % sprite_page_size;
			size_t sprite_page_y = sprite_page_off / 256;
			size_t sprite_page_x = sprite_page_off % 256;
			uint16_t virt_palette = style.palette_base.sprite + sprite_id;

			glPushMatrix();

			f4x4 translate = f4x4_translate(car.x, car.y, car.z);
			f4x4 rotate = f4x4_rotate(car.angle + M_PI/2., f3_make(0.0f, 0.0f, 1.0f));
			glMultMatrixf(f4x4_mul(translate, rotate).e);
			f4x4 translate_inv;
			f4x4_invert(translate, &translate_inv);
			glUniformMatrix4fv(loc_frame_to_canonical, 1, false, translate.e);

			glActiveTexture(GL_TEXTURE0);
			glBindTexture(GL_TEXTURE_2D, style.sprite_pages[sprite_page]);
			glActiveTexture(GL_TEXTURE1);
			uint16_t palette_id = style.palette_index.phys_palette[virt_palette];
			glBindTexture(GL_TEXTURE_2D, style.palette_pages[palette_id / 64]);
			glUniform1f(glGetUniformLocation(pipeline_program, "palette_column"),
			            (palette_id % 64 + 0.5f) / 64.0f);
			glUniform1f(glGetUniformLocation(pipeline_program, "min_alpha"), 0.0f);

			float width = sprite->w / 64.0f;
			float breadth = sprite->h / 64.0f;

			// TODO: Improve this calculation, currently we truncate a pixel.
			float tex_top_x = (sprite_page_x + 1.0f) / 255.0f;
			float tex_top_y = (sprite_page_y + 1.0f)/ 255.0f;
			float tex_bottom_x = (sprite_page_x + sprite->w - 1.0) / 255.f;
			float tex_bottom_y = (sprite_page_y + sprite->h - 1.0) / 255.f;

			glBegin(GL_QUADS);
			glNormal3f(0.0, 0.0, 1.0f);
			glTexCoord2f(tex_bottom_x, tex_bottom_y);
			glVertex3f(+width/2., +breadth/2., 0.1);
			glTexCoord2f(tex_top_x, tex_bottom_y);
			glVertex3f(-width/2., +breadth/2., 0.1);
			glTexCoord2f(tex_top_x, tex_top_y);
			glVertex3f(-width/2., -breadth/2., 0.1);
			glTexCoord2f(tex_bottom_x, tex_top_y);
			glVertex3f(+width/2., -breadth/2., 0.1);
			glEnd();

			glPopMatrix();
		}
	}
	glPopMatrix();

	glPopMatrix();

	glUseProgramObjectARB(0);
}

// Prepare the rendering pipeline.
static bool InitializeRender()
{
	// Enable vertical sync.
	SDL_GL_SetAttribute(SDL_GL_SWAP_CONTROL, 1);

	// Enable Z-buffer.
	glEnable(GL_DEPTH_TEST);

	return true;
}

// Update the simulation numerically.
static void Think(float deltatime)
{
	UpdateSimulation(deltatime);
}

// Render the simulation to the framebuffer.
static void Render()
{
	RenderSimulation();
	SDL_GL_SwapBuffers();
}

// Continuously update and render the simulation.
static int Mainloop()
{
	SDL_Surface* newscreen;

	// Render the initial state of the world before letting it think.
	uint32_t lastmsec = SDL_GetTicks();
	Render();

	int ret = 0;
	bool running = true;
	while ( running )
	{
		// Calculate the time since last frame and don't render too fast.
		const uint32_t minframetime = 1;
		uint32_t nowmsec;
		while ( (nowmsec = SDL_GetTicks()) - lastmsec < minframetime )
			SDL_Delay(lastmsec + minframetime - nowmsec);
		uint32_t deltamsec = nowmsec - lastmsec;
		lastmsec = nowmsec;
		float deltatime = (float) deltamsec / 1000.0f;

		// Handle various SDL events such as keystrokes.
		SDL_Event event;
		while ( SDL_PollEvent(&event) ) switch ( event.type )
		{
		case SDL_QUIT:
			running = false;
			break;
		case SDL_KEYUP:
			if ( event.key.keysym.sym == SDLK_ESCAPE )
				running = false;
			if ( event.key.keysym.sym == SDLK_LEFT )
				key_left = false;
			if ( event.key.keysym.sym == SDLK_RIGHT )
				key_right = false;
			if ( event.key.keysym.sym == SDLK_UP )
				key_up = false;
			if ( event.key.keysym.sym == SDLK_DOWN )
				key_down = false;
			break;
		case SDL_KEYDOWN:
			if ( event.key.keysym.sym == SDLK_LEFT )
				key_left = true;
			if ( event.key.keysym.sym == SDLK_RIGHT )
				key_right = true;
			if ( event.key.keysym.sym == SDLK_UP )
				key_up = true;
			if ( event.key.keysym.sym == SDLK_DOWN )
				key_down = true;
			if ( event.key.keysym.sym == SDLK_RETURN )
				is_driving = !is_driving;
			break;
		case SDL_VIDEORESIZE:
			// Attempt to update our OpenGL context in case of a resize.
			newscreen = SDL_SetVideoMode(event.resize.w, event.resize.h,
			                             screen_bpp, sdl_video_flags);
			if ( !newscreen )
			{
				fprintf(stderr, "Warning: unable to update sdl surface after "
				                "resize, window manager and opengl may "
				                "disagree the framebuffer resolution.\n");
				break;
			}
			// Remember the new resolution.
			screen_width = event.resize.w;
			screen_height = event.resize.h;
			sdl_screen = newscreen;
			break;
		}

		// Update the simulation.
		Think(deltatime);

		// Render the simuation.
		Render();
	}
	return ret;
}

int main(int argc, char* argv[])
{
	srand(time(NULL));

	bool fullscreen = false;

	if ( argc != 3 )
	{
		printf("Usage: %s MAP-FILE STYLE-FILE\n", argv[0]);
		exit(0);
	}

	// Initialize Simple Directmedia Layer (SDL).
	if ( SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) )
	{
		fprintf(stderr, "unable to initialize sdl: %s", SDL_GetError());
		exit(1);
	}

	// Figure out what resolution to use.
	const SDL_VideoInfo* videoinfo = SDL_GetVideoInfo();
	if ( screen_width == -1 )
		screen_width = fullscreen ? videoinfo->current_w
		                          : DEFAULT_WINDOW_WIDTH;
	if ( screen_height == -1 )
		screen_height = fullscreen ? videoinfo->current_h
		                           : DEFAULT_WINDOW_HEIGHT;
	if ( screen_bpp == -1 )
		screen_bpp = videoinfo->vfmt->BitsPerPixel;
	sdl_video_flags = SDL_HWSURFACE | SDL_DOUBLEBUF | SDL_OPENGL;
	if ( fullscreen )
		sdl_video_flags |= SDL_FULLSCREEN;
	else
		sdl_video_flags |= SDL_RESIZABLE;

	// Create the window in which we will display.
	if ( !(sdl_screen = SDL_SetVideoMode(screen_width, screen_height,
	                                     screen_bpp, sdl_video_flags)) )
	{
		fprintf(stderr, "can't create sdl surface: %s", SDL_GetError());
		exit(1);
	}

	SDL_WM_SetCaption(argv[0], NULL);

	// Cleanup SDL on program exit.
	atexit(SDL_Quit);

	// Initialize the OpenGL Extension Wrangler Library.
	GLenum glew_err = glewInit();
	if ( glew_err != GLEW_OK )
		error(1, 0, "glew initization failed: %s", glewGetErrorString(glew_err));

	load_level(argv[1], &level);
	load_style(argv[2], &style);

	car.x = level.spawn_x;
	car.y = level.spawn_y;

	if ( !InitializeRender() || !InitializeSimulation() )
		exit(1);

	return Mainloop();
}
