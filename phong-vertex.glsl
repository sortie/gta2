#version 130

varying vec4 color_vertex;
varying vec3 normal_vertex;
varying vec4 position_vertex;
varying vec4 world_vertex;
varying vec2 texture_coordinate;

uniform mat4 frame_to_canonical;
uniform vec4 light_source_position[32];
uniform vec4 light_source_diffuse[32];

void main()
{
	color_vertex = gl_Color;
	normal_vertex = normalize(gl_NormalMatrix * gl_Normal);

	position_vertex = /*gl_ModelViewMatrix * */ gl_Vertex;
	world_vertex = frame_to_canonical * gl_Vertex;
	texture_coordinate = vec2(gl_MultiTexCoord0);

	gl_Position = gl_ModelViewProjectionMatrix * gl_Vertex;
}
