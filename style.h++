#ifndef STYLE_HXX
#define STYLE_HXX

struct palette_index
{
	uint16_t phys_palette[16384];
};

struct palette_base
{
	uint16_t tile;
	uint16_t sprite;
	uint16_t car_remap;
	uint16_t ped_remap;
	uint16_t code_obj_remap;
	uint16_t map_obj_remap;
	uint16_t user_remap;
	uint16_t font_remap;
};

struct sprite_entry
{
	uint32_t ptr;
	uint8_t w;
	uint8_t h;
	uint16_t pad;
};

struct sprite_base
{
	uint16_t car;
	uint16_t ped;
	uint16_t code_obj;
	uint16_t map_obj;
	uint16_t user;
	uint16_t font;
};

struct delta_entry
{
	uint16_t which_sprite;
	uint8_t delta_count;
	uint8_t pad;
	uint16_t delta_size[/*delta_count*/];
};

struct font_base
{
	uint16_t font_count;
	uint16_t base[/*font_count*/];
};

struct door_info
{
	int8_t rx;
	int8_t ry;
};

const uint8_t CAR_RATING_BAD = 1;
const uint8_t CAR_RATING_BAD_X2 = 2;
const uint8_t CAR_RATING_BAD_X3 = 3;
const uint8_t CAR_RATING_AVERAGE = 11;
const uint8_t CAR_RATING_AVERAGE_X2 = 12;
const uint8_t CAR_RATING_AVERAGE_X3 = 13;
const uint8_t CAR_RATING_GOOD = 21;
const uint8_t CAR_RATING_GOOD_X2 = 22;
const uint8_t CAR_RATING_GOOD_X3 = 23;

const uint8_t INFO_FLAG_PED_JUMP = 1 << 0;
const uint8_t INFO_FLAG_EMERG_LIGHTS = 1 << 1;
const uint8_t INFO_FLAG_ROOF_LIGHTS = 1 << 2;
const uint8_t INFO_FLAG_CAB = 1 << 3;
const uint8_t INFO_FLAG_TRAILER = 1 << 4;
const uint8_t INFO_FLAG_FOREHIRE_LIGHTS = 1 << 5;
const uint8_t INFO_FLAG_ROOF_DECAL = 1 << 6;
const uint8_t INFO_FLAG_REAR_EMERG_LIGHTS = 1 << 7;
const uint8_t INFO_FLAG_2_COLLIDE_OVER = 1 << 0;
const uint8_t INFO_FLAG_2_POPUP = 1 << 1;

struct car_info
{
	uint8_t model;
	uint8_t sprite;
	uint8_t w;
	uint8_t h;
	uint8_t num_remaps;
	uint8_t passengers;
	uint8_t wreck;
	uint8_t rating;
	int8_t front_wheel_offset;
	int8_t rear_wheel_offset;
	int8_t front_window_offset;
	int8_t rear_window_offset;
	uint8_t info_flags;
	uint8_t info_flags_2;
	uint8_t remap[/*num_remaps*/];
	uint8_t num_doors;
	door_info doors[/*num_doors*/];
};

struct object_info
{
	uint8_t model;
	uint8_t sprites;
};

const char STYLE_FILE_HEADER_TYPE[4] = { 'G', 'B', 'S', 'T' };
const uint16_t STYLE_FILE_HEADER_VERSION = 600;

const char CHUNK_TYPE_PALB[4] = { 'P', 'A', 'L', 'B' };
const char CHUNK_TYPE_PALX[4] = { 'P', 'A', 'L', 'X' };
const char CHUNK_TYPE_PPAL[4] = { 'P', 'P', 'A', 'L' };
const char CHUNK_TYPE_SPRB[4] = { 'S', 'P', 'R', 'B' };
const char CHUNK_TYPE_SPRG[4] = { 'S', 'P', 'R', 'G' };
const char CHUNK_TYPE_SPRX[4] = { 'S', 'P', 'R', 'X' };
const char CHUNK_TYPE_TILE[4] = { 'T', 'I', 'L', 'E' };

#endif
