#version 130

varying vec4 color_vertex;
varying vec3 normal_vertex;
varying vec4 position_vertex;
varying vec4 world_vertex;
varying vec2 texture_coordinate;

uniform vec4 light_source_position[32];
uniform vec4 light_source_diffuse[32];
uniform float light_source_radius[32];
uniform sampler2D texture;
uniform sampler2D palette;
uniform float palette_column;
uniform float min_alpha;

vec4 paletteFetch(ivec2 pix)
{
	float entry = texelFetch(texture, pix, 0).r;
	return texture2D(palette, vec2(palette_column, entry));
}

void main()
{
	vec3 normal = normalize(normal_vertex);
	vec4 mdl_pos = world_vertex;

	vec4 light_color = vec4(0.0, 0.0, 0.0, 0.0);
	for ( int index = 0; index < 32; index++ )
	{
		vec3 lightdir_vertex = normalize(light_source_position[index].xyz - light_source_position[index].w * mdl_pos.xyz);
		vec3 lightdir = normalize(lightdir_vertex);

		// Lit the backside of surfaces so the roof also get a little light.
		float cos_angle_normal = dot(normal, lightdir_vertex);
		if ( cos_angle_normal < 0.0 )
			cos_angle_normal *= -0.5;

		float lightdir_dist = distance(world_vertex, light_source_position[index]);
		float radius = light_source_radius[index];
		float light_dist = lightdir_dist / radius;
		// HACK: Prevent things from getting too bright.
		if ( light_dist < 1.5 )
			light_dist = 1.5;
		vec4 diffuse = light_source_diffuse[index] * cos_angle_normal / (light_dist * light_dist);

		light_color += diffuse;
	}

	light_color.xyz += vec3(0.15, 0.15, 0.15);
	light_color.a = 1.0;

	ivec2 texture_size = textureSize(texture, 0);
	vec2 scaled_texture_coordinate = vec2(texture_coordinate.x * (texture_size.x-1),
	                                      texture_coordinate.y * (texture_size.y-1));

	ivec2 topLeft = ivec2(floor(scaled_texture_coordinate));
	ivec2 bottomRight = topLeft + ivec2(1, 1);

	vec4 pixTL = paletteFetch(topLeft);
	vec4 pixTR = paletteFetch(ivec2(bottomRight.x, topLeft.y));
	vec4 pixBL = paletteFetch(ivec2(topLeft.x, bottomRight.y));
	vec4 pixBR = paletteFetch(bottomRight);
	vec4 pixT = mix(pixTL, pixTR, fract(scaled_texture_coordinate.x));
	vec4 pixB = mix(pixBL, pixBR, fract(scaled_texture_coordinate.x));
	vec4 texture_color = mix(pixT, pixB, fract(scaled_texture_coordinate.y));

	gl_FragColor = color_vertex * light_color * texture_color;
	gl_FragColor.a = max(min_alpha, gl_FragColor.a);
}
